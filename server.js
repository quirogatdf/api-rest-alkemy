const express = require('express')
const cors = require('cors');
const helmet = require('helmet');


function makeServer() {
  /* Configuramos nuestro servidor Express*/
  const app = express();
  app.use(cors());
  app.use(helmet());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  const apiRouter = require('./src/routes/index');
  app.use('/api', apiRouter)
  return app;
}

module.exports = {
  makeServer,
};