const { Sequelize } = require('sequelize');
const { DB } = require('../config');

const connection = new Sequelize({
  database: process.env.MYSQL_DATABASE,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  dialect: process.env.MYSQL_DIALECT,
  logging: false,
  query: { raw: true },
});

module.exports = { connection };
