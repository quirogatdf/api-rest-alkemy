const { Router } = require ('express')
const router = Router();
const PersonajesControllers = require('../controllers/personaje.controllers');

router
      .get('/', PersonajesControllers.getAll)
      .post('/add', PersonajesControllers.create)
      .delete('/:id/delete', PersonajesControllers.delete)

module.exports = router;
