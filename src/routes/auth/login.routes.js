const { Router } = require ('express')
const LoginController = require('../../controllers/auth/auth.controllers')
const { verifyNull, verifiyMailDuplicate } = require('../../middlewares/auth.middleware')

const router = Router();

router
      .post('/login',verifyNull, LoginController.login)
      .post('/register',verifyNull, verifiyMailDuplicate, LoginController.register)


module.exports = router;
