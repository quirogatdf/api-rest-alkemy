const router = require('express').Router();
const loginRoutes = require('./auth/login.routes.js');
const personajesRoutes = require('./personajes.routes');
router
    .use('/auth',loginRoutes)
    .use('/characters', personajesRoutes)
    
module.exports = router
