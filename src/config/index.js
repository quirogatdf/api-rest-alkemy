require ('dotenv').config();

module.exports = {
  SERVER: {
    PORT: process.env.SERVER_PORT || 3000,
  },
  DB: {
    PORT     : process.env.MYSQL_PORT,
    HOST     : process.env.MYSQL_HOST,
    DATABASE : process.env.MYSQL_DATABASE,
    PASSWORD : process.env.MYSQL_PASSWORD,
    USER     : process.env.MYSQL_USER,
    DIALECT  : process.env.MYSQL_DIALECT
  },
  JWT : {
    KEY: process.env.JSON_KEY,
    EXPIRES_TIME: process.env.JSON_EXPIRES_TIME
  },
}