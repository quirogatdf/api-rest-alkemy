
const { connection } = require('../database/db')

exports.getAllDB = async () => {
  return await connection.query('SELECT * FROM personaje', { type: connection.QueryTypes.SELECT })
}

exports.getOneById = async (id) => {
  return await connection.query('SELECT * FROM personaje WHERE id = ?', {
    type: sequelize.QueryTypes.SELECT,
    replacements: [id]
  });
}

exports.Add = async (personaje) => {
  return await connection.query(
    'INSERT INTO personaje (id, image, name, age, weight, history) VALUES (null, ?, ?, ?, ?, ?)',
    {
      type: connection.QueryTypes.INSERT,
      replacements: [personaje.image, personaje.name, personaje.age, personaje.weight, personaje.history]
    })
}
exports.deleteById = async (id) => {
  return await connection.query('DELETE FROM personaje WHERE id = ?', {
    replacements: [id]
  })

}