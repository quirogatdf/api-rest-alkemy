const { JWT } = require('../../config');
const { sign } = require('jsonwebtoken');
const db = require('../../models/index');


/* Control de inicio de sesión */
exports.login = async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  User = db.getModel('User');
  const data = await User.findOne({
    where: {
      username: username,
      //password: password
    }, raw: true
  })

  if (data) {
    if (data.password === password) {
      /* Generar Token */
      const newToken = sign(data, JWT.KEY, {
        expiresIn: JWT.EXPIRES_TIME
      })
      /* Guardar Token en la BD */
      await User.update({
        token: newToken
      }, {
        where: { id: data.id }
      })
      res.send({ auth: true, message: `Bienvenido, ${data.lastname}, ${data.name}`, token: newToken });
    } else {
      res.send({ auth: false, message: 'Contraseña inválida' })
    }
  } else {
    res.status(400).json({ auth: false, message: 'Usuario Inválido' })

  }
}

/* Registrar un usuario nuevo */
exports.register = async (req, res) => {
  const { username, name, lastname, email, password} = req.body
  try {
    User = db.getModel('User');
    let data = await User.create(req.body)
    res.status(201).json({ message: `El usuario ${data.username}, se ha creado con éxito` })
  } catch (error) {
    res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
  }

}

