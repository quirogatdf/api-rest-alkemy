const PersonajesService = require('../services/personajes.services');

exports.getAll = async (req, res) => {
  try {
    const data = await PersonajesService.getAllDB();
    res.send(data);
  } catch (error) {
    res.status(500).json({ error: 'Something went wrong. Please retry or contact with an admin.', message: error })
  }

};

exports.create = async (req, res) => {
  const { image, name, age, weight, history } = req.body

  try {
    const data = await PersonajesService.Add({ image, name, age, weight, history });
    console.log(`Character Added to database ${data}`)
    res.status(200).json({ message: 'Character created successfully.' })
  } catch (error) {
    res.status(500).json({ error: 'Something went wrong. Please retry or contact with an admin.', message: error })
  }
}

exports.delete = async (req, res) => {
  const id = parseInt(req.params.id)

  if (!isNaN(id)) {

    try {
      const character = await PersonajesService.getOneById(id)
      console.log(character);
      if (character[0]) {

        await PersonajesService.deleteById(id)
        res.status(200).json({ message: `Product with id ${id} was disabled correctly` });

      } else {

        res.status(404).json({ message: 'Not found id' })
      }

    } catch (error) {

      res.status(500).json({ error: 'Something went wrong. Please retry or contact with an admin.', message: error })
    }

  } else {

    res.status(402).send({ error: 'Bad request.', message: 'Id must be a number' })
  }

}