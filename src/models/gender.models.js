const { DataTypes } = require('sequelize');

function createModel(connection) {
  const Gender = connection.define('Gender', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
    },
    image: {
      type: DataTypes.STRING,
    }
  }, {
    tableName: 'Gender',
    timestamps: false
  });
  return Gender;
}

module.exports = {
  createModel,
};