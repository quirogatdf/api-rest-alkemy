const { DataTypes } = require('sequelize');

function createModel(connection) {
  const Movie = connection.define('Movie', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    image: {
      type: DataTypes.STRING,
    },
    title: {
      type: DataTypes.STRING,
    },
    creation_date: {
      type: DataTypes.DATE,
    },
    rating: {
      type: DataTypes.INTEGER,
    },
  }, {
    tableName: 'Movie',
    timestamps: false
  });
  return Movie;
}

module.exports = {
  createModel,
};