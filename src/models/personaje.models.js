const { DataTypes } = require('sequelize');

function createModel(connection) {
  const Personaje = connection.define('Personaje', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    image: {
      type: DataTypes.STRING,
    },
    name: {
      type: DataTypes.STRING,
    },
    age: {
      type: DataTypes.INTEGER,
    },
    weight: {
      type: DataTypes.DOUBLE,
    },
    history: {
      type: DataTypes.STRING(500),

    }
  }, {
      tableName: 'Personaje',
      timestamps: false
  });
  return Personaje;
}

module.exports = {
  createModel,
};