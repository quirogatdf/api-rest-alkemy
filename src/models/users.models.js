const { DataTypes } = require('sequelize');

function createModel(connection) {
  const User = connection.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    username: {
      type: DataTypes.STRING,
    },
    name: {
      type: DataTypes.STRING,
    },
    lastname: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,

    }
  }, {
    tableName: 'User',
    timestamps: false
  });
  return User;
}

module.exports = {
  createModel,
};