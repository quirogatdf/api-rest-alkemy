const { connection } = require('../database/db');
const { createModel: createPersonajeModel } = require('./personaje.models');
const { createModel: createGenderModel } = require('./gender.models');
const { createModel: createMovieModel } = require('./movie.models');
const { createModel: createUserModel } = require('./users.models');

const models = {};

async function connect() {

  models.Personaje = createPersonajeModel(connection);
  models.Movie = createMovieModel(connection);
  models.Gender = createGenderModel(connection);
  models.User = createUserModel(connection);
  
  try {
    await connection.authenticate();
    await connection.sync({force: false});
    console.log('Connection has been established successfully.');
    
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
  
}
function getModel(name) {
  if (!models[name]) {
    global.console.log('No existe');
    return null;
  }
  return models[name];
}

module.exports = {
  connect,
  getModel,
};